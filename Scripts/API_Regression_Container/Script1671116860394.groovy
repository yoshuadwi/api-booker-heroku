import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WS.sendRequestAndVerify(findTestObject('Ping'))

WS.sendRequestAndVerify(findTestObject('Auth/WrongPassword_Login'))

WS.sendRequestAndVerify(findTestObject('Auth/WrongUsername_Login'))

def response = WS.sendRequest(findTestObject('Auth/Success_Login'))

token = WS.getElementPropertyValue(response, 'token')

GlobalVariable.token = token

WS.sendRequestAndVerify(findTestObject('Booking/Create/NoFirstName- Create New Book'))

WS.sendRequestAndVerify(findTestObject('Booking/Create/WrongDateFormat - Create New Book'))

WS.sendRequestAndVerify(findTestObject('Booking/Create/WrongFormatPrice- Create New Book'))

WS.sendRequestAndVerify(findTestObject('Booking/Create/WrongInputDate- Create New Book'))

response = WS.sendRequest(findTestObject('Booking/Create/Create New Book'))

id = WS.getElementPropertyValue(response, 'bookingid')

WS.sendRequestAndVerify(findTestObject('Booking/GetData/GetBookingId'))

WS.sendRequestAndVerify(findTestObject('Booking/GetData/GetBookingIDByDate'))

WS.sendRequestAndVerify(findTestObject('Booking/GetData/GetBookingIDByNames'))

WS.sendRequestAndVerify(findTestObject('Booking/GetData/WrongFormat_GetBookingIDByDate'))

WS.sendRequestAndVerify(findTestObject('Booking/GetData/GetBookingByID', [('id') : id]))

WS.sendRequestAndVerify(findTestObject('Booking/Update/Failed_MissingField--UpdateBooking', [('id') : id, ('token') : token]))

WS.sendRequestAndVerify(findTestObject('Booking/Update/Failed_WrongToken--UpdateBooking', [('id') : id, ('token') : '1234567dvbn']))

WS.sendRequestAndVerify(findTestObject('Booking/Update/PartialUpdateBooking', [('id') : id, ('token') : token]))

WS.sendRequestAndVerify(findTestObject('Booking/Update/UpdateBooking', [('id') : id, ('token') : token]))

WS.sendRequestAndVerify(findTestObject('Booking/Delete/DeleteBooking',, [('id') : id]))

